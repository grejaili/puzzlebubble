﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LauncherController : MonoBehaviour
{
    // Start is called before the first frame update
    public Ball activeBall;
    public float velocity;

    [ContextMenu("Launch Ball")]
    public void LaunchBall()
    {
        activeBall.LaunchBall(-velocity);
    }

    public void RotateLeft()
    {
        activeBall.transform.Rotate(0, 90, 0);
    }

    private void Update()
    {
        RotateRight();
    }

    public void RotateRight()
    {
        activeBall.transform.Rotate(0, Time.deltaTime * 1, 0, Space.Self);
    }

    public void setActiveBall(Ball nextBall)
    {
        activeBall = nextBall;
        activeBall.rb.isKinematic = false;
    }

}
