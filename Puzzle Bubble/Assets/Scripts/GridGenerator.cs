﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class GridGenerator : MonoBehaviour
{

    public Grid grid;

    public GameObject FloorPrefab;
    public GameObject BorderPrefab;

    public Vector2 gridSize;
    public List<GameObject> matrix;


    private void Awake()
    {
        GameVariables.gridSize = gridSize;
    }




    [ContextMenu("Generate Grid")]
    void GenerateGrid()
    {

        DestroyGrid();
        GameVariables.gridSize = gridSize;
        Debug.Log(gridSize);
        for(int i = 0; i <= gridSize.x; i++)
        {
            for(int j = 0; j <= gridSize.y; j++)
            {
                GameObject aux;


                if(i.Equals(0) || j.Equals(0))
                {
                    aux = Instantiate(BorderPrefab) as GameObject;
                }
                else if(i == gridSize.x || j == gridSize.y)
                {

                    aux = Instantiate(BorderPrefab) as GameObject;
                }
                else
                {
                    aux = Instantiate(FloorPrefab) as GameObject;
                }



                aux.transform.SetParent(this.transform);

                aux.transform.position = grid.CellToLocal(new Vector3Int(i, 0, j));
                //Debug.Log("i " + i + " j " + j);
                matrix.Add(aux);
            }
        }
    }


    [ContextMenu("Destroy Grid")]
    void DestroyGrid()
    {

        foreach(GameObject i in matrix)
        {
            DestroyImmediate(i);
        }

        matrix.Clear();
    }



}
