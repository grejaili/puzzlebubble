﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public List<Ball> neighboors;
    public BallCollor myCollor;
    [SerializeField] MeshRenderer myRenderer;
    public Rigidbody rb;
    Vector3 velocity;

    void Start()
    {
        FindNeighboors(this.transform.position, 1.1f);
        GenerateNewBall();
    }

    private void FixedUpdate()
    {

    }

    void GenerateNewBall()
    {
        myCollor = (BallCollor)Random.Range(0, 3);
        changeCollor(myCollor.ToString());
    }

    void changeCollor(string color)
    {

        switch(color)
        {
            case "red":
            myRenderer.material.color = Color.red;
            break;
            case "blue":
            myRenderer.material.color = Color.blue;
            break;
            case "green":
            myRenderer.material.color = Color.green;
            break;
        }
    }


    void FindNeighboors(Vector3 center, float radius)
    {
        Collider[] hitColliders = Physics.OverlapSphere(center, radius);
        foreach(Collider i in hitColliders)
        {
            if(i.CompareTag("Ball"))
            {
                if(i.TryGetComponent(out Ball ball))
                {
                    if(ball != this)
                    {
                        if(ball.myCollor == myCollor)
                            neighboors.Add(ball);
                    }
                }
            }
        }
    }


    public void LaunchBall(float newVelocity)
    {
        // velocity = transform.forward * newVelocity;
        rb.AddForce(transform.forward * newVelocity);
    }


    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Ball"))
        {
            rb.isKinematic = true;
            rb.velocity = Vector3.zero;
            if(collision.gameObject.TryGetComponent(out Ball hit))
            {
                if(hit.myCollor.Equals(myCollor))
                {
                    Debug.Log("Check Destruction Sequence");
                }
            }


        }

    }

    public void NewRotation()
    {

    }

    public enum BallCollor
    {
        red,
        blue,
        green,

    }

}
