﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] GameObject balls;
    [SerializeField] Grid grid;
    [SerializeField] Vector2 NumberOfBalls;
    [SerializeField] Vector2 RandomOffSet;
    [SerializeField] Vector3Int BallSpawningPos;
    public LauncherController launcherController;
    // Start is called before the first frame update
    void Start()
    {
        placeBalls();
        SpawnNextBall();
    }


    void SpawnNextBall()
    {
        GameObject ballprefab = Instantiate(balls) as GameObject;
        ballprefab.transform.SetParent(this.transform);
        ballprefab.transform.position = grid.CellToLocal(BallSpawningPos);  
        launcherController.setActiveBall(ballprefab.GetComponent<Ball>());

    }


    void placeBalls()
    {
        float rowsBalls;

        for(int j = 0; j < NumberOfBalls.x; j++)
        {
            rowsBalls = Random.Range(3, NumberOfBalls.y);
            int RandomShake = Mathf.FloorToInt(Random.Range(RandomOffSet.x, RandomOffSet.y));
            for(int i = 0; i < rowsBalls; i++)
            {
                GameObject ballprefab = Instantiate(balls) as GameObject;
                ballprefab.transform.SetParent(this.transform);
                ballprefab.transform.position = grid.CellToLocal(
                    new Vector3Int(
                        i + RandomShake + Mathf.RoundToInt(GameVariables.gridSize.x / 4)
                        , 1, j + 1));
            }
        }
    }


}
